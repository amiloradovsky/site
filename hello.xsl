<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/hello">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <link rel="stylesheet" type="text/css" href="/hello.css" />
        <title> CGI+XML test </title>
        <style>
          h1 {
            color: green;
          }
        </style>
      </head>
      <body>

        <section>
          <h1> Arguments: </h1>
          <ul>
            <xsl:for-each select="arguments/argument">
              <li> <xsl:value-of select="."/> </li>
            </xsl:for-each>
          </ul>
        </section>

        <section>
          <h1> Environment: </h1>
          <ul>
            <xsl:for-each select="environment/slot">
              <li> <xsl:value-of select="."/> </li>
            </xsl:for-each>
          </ul>
        </section>

        <section>
          <h1> Form fields: </h1>
          <ul>
            <xsl:for-each select="fields/field">
              <li> <xsl:value-of select="."/> </li>
            </xsl:for-each>
          </ul>
        </section>

        <section>
          <h1> Greeting: </h1>
          <p> <xsl:value-of select="greeting"/> </p>
        </section>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
